<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>
<section class="hero" style='background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)'>
  <div class="container">
    <div class="hero-wrapper">
      <?php the_content(); ?>
      <div class="hero-buttons">
        <a class="button btn-hero1" href="<?php the_field('link_1'); ?>" target="_blank"><?php the_field('button_1'); ?></a>
        <a class="button btn-hero2" href="<?php the_field('link_2'); ?>" target="_blank"><?php the_field('button_2'); ?></a>
      </div>
    </div>
  </div>
  <!-- container -->
</section>
<!-- hero -->
<?php endwhile; ?>

<!-- Cloud platform logo -->
<section class="cloud-platform">
  <div class="container">
    <?php dynamic_sidebar('cloud_section') ?>
  </div>
</section>
<!-- cloud-platform -->


<!-- Serverless Development -->
<section class="serverless-devs">
  <div class="container">
    <?php
      $args = array(
        'post_type' => 'serverless',
        'posts_per_page' => 4,
        'order' => 'DESC',
        'orderby' => 'date'
      );

      $serverless = new WP_Query($args);
      while($serverless->have_posts()): $serverless->the_post();
     ?>
    <div class="serverless-wrapper">
      <div class="serverless-img">
        <?php the_post_thumbnail(); ?>
      </div>
      <div class="serverless-text">
        <h3><?php the_title(); ?></h3>
        <?php the_content(); ?>
      </div>
    </div>
    <?php endwhile; wp_reset_postdata();  ?>
  </div>
</section>
<!-- serverless-devs  -->


<!-- Cloud Deployment -->
<section class="cloud-deployment">
  <div class="container">
			<?php
				$args=array(
					'post_type' => 'cloud',
					'posts_per_page' => 1
				);

				$cloud = new WP_Query($args);
				while($cloud->have_posts()): $cloud->the_post();
			?>
				<h2><?php the_title(); ?></h2>
			<div class="cloud-wrapper">
				<div class="cloud-1">
					<h3><?php the_field('title_1'); ?></h3>
					<p><?php the_field('description_1'); ?></p>
					<a href="#"><?php the_field('button_1'); ?></a>
				</div>
				<!-- cloud-1 -->
				<div class="cloud-2">
					<h3><?php the_field('title_2'); ?></h3>
					<p><?php the_field('description_2'); ?></p>
					<a href="#"><?php the_field('button_2'); ?></a>
				</div>
				<!-- cloud-2 -->
				<div class="cloud-3">
					<h3><?php the_field('title_3'); ?></h3>
					<p><?php the_field('description_3'); ?></p>
					<a href="#"><?php the_field('button_3'); ?></a>
				</div>
				<!-- cloud-3 -->
				<div class="cloud-4">
					<h3><?php the_field('title_4'); ?></h3>
					<p><?php the_field('description_4'); ?></p>
					<a href="#"><?php the_field('button_4'); ?></a>
				</div>
				<!-- cloud-4 -->
			</div>
			<?php endwhile; wp_reset_postdata(); ?>	
  </div>
</section>
<!-- cloud-deployment -->

<!-- Clients -->
<section class="clients">
  <div class="container">
		<?php  
			$args = array(
				'post_type' => 'clients'
			);

			$clients = new WP_Query($args);
			while($clients->have_posts()): $clients->the_post();
		?>
		<div class="clients-wrapper">
			<h2><?php the_title(); ?></h2>
			<img src="<?php the_field('clients') ?>" alt="Clients logo">
		</div>
		<?php endwhile; wp_reset_postdata(); ?>
	</div>
</section>
<!-- clients -->


<!-- Testimonials -->
<section class="testimonials">
  <div class="container">
		<?php
			$args = array(
				'post_type' => 'testimonials'
			);

			$testimonials = new WP_Query($args);
			while($testimonials->have_posts()): $testimonials->the_post();
		?>
			<div class="testimonials-wrapper">
				<div class="testimonial">
					<div class="testimonial-text">
						<?php the_content(); ?>
					</div>
					<div class="testimonial-person">
						<div class="testimonial-img">
							<img src="<?php the_field('picture'); ?>" alt="Person image">
						</div>
						<div class="testimonial-name">
							<p><?php the_field('name'); ?></p>
							<p><?php the_field('title'); ?></p>
							<p><?php the_field('company'); ?></p>						
						</div>
					</div>
				</div>
				<!-- testimonial -->

				<div class="testimonial-video">
					<a href="#">
						<?php the_post_thumbnail(); ?>
					</a>
					<img src="<?php echo get_template_directory_uri(); ?>/img/play_button.svg" class="play-button" alt="Play button">
				</div>
			</div>
		<?php endwhile; wp_reset_postdata(); ?>
	</div>
</section>
<!-- testimonials -->

<!-- Get Started -->
<section class="get-started">
	<div class="container">
		<?php
			$args = array(
				'post_type' => 'started'
			);

			$started = new WP_Query($args);
			while($started->have_posts()): $started->the_post();
		?>
			<h2><?php the_title(); ?></h2>
			<div class="get-started-wrapper">
				<div class="get-started-1">
					<img src="<?php the_field('start_icon_1'); ?>" alt="Icon 1">
					<h4><?php the_field('start_title_1'); ?></h4>
					<p><?php the_field('start_content_1'); ?></p>
				</div>
				<div class="get-started-2">
					<img src="<?php the_field('start_icon_2'); ?>" alt="Icon 2">
					<h4><?php the_field('start_title_2'); ?></h4>
					<p><?php the_field('start_content_2'); ?></p>
				</div>
				<div class="get-started-3">
					<img src="<?php the_field('start_icon_3'); ?>" alt="Icon 3">
					<h4><?php the_field('start_title_3'); ?></h4>
					<p><?php the_field('start_content_3'); ?></p>
				</div>

			</div>
			<!-- get-started-wrapper -->
		<?php endwhile; wp_reset_postdata(); ?>
	</div>
</section>
<!-- get-started -->


<?php get_footer(); ?>