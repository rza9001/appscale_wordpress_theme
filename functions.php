<?php

if (function_exists('add_theme_support'))
{
  // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
}


// Add custom logo
function appscale_custom_logo() {
	$logo = array(
		'height' => 60,
		'width' => 218
	);
	add_theme_support('custom-logo', $logo);
}
add_action('after_setup_theme', 'appscale_custom_logo');


// Add Menus
function appscale_menus() {
  register_nav_menus(array(
    'main-menu' => __('Main Menu', 'appscale'), 
    'social-menu' => __('Social Menu', 'appscale')
  ));
}
add_action('init', 'appscale_menus');



function appscale_styles() {
  wp_register_style('latoFont', 'https://fonts.googleapis.com/css?family=Lato:300,400,700', array(), '1.0.0');
  wp_enqueue_style('latoFont');

  wp_register_style('style', get_template_directory_uri(). '/style.css', array(), '1.0');
  wp_enqueue_style('style');
}
add_action('wp_enqueue_scripts', 'appscale_styles');


function appscale_scripts() {
  wp_register_script('script', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true); 
  wp_register_script('fontAwesome', get_template_directory_uri() . '/js/font_awesome.all.min.js', array(), '5.11.2', true); 
  
  wp_enqueue_script('jquery');
  wp_enqueue_script('fontAwesome');
  wp_enqueue_script('script');
}
add_action('wp_enqueue_scripts', 'appscale_scripts');


// Add widget zone
function appscale_cloud_widgets() {
	register_sidebar(array(
		'name' => 'Cloud Platforms',
		'id' => 'cloud_section',
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}
add_action('widgets_init', 'appscale_cloud_widgets');









?>