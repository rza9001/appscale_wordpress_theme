    <footer>
        <div class="footer-1">
            <div class="footer-menu">
              <?php wp_nav_menu(array(
                'theme_location' => 'main-menu'
              )); ?>
            </div>  
            <!-- footer-menu -->

            <?php
              $args = array(
                'post_type' => 'socmed'
              );

              $socmed = new WP_Query($args);
              while($socmed->have_posts()): $socmed->the_post();
            ?>
              <div class="social-icons">
                <a href="<?php the_field('social_media_1');  ?>" target="_blank">
                  <i class="<?php the_field('social_media_icon_1'); ?>"></i>
                </a>
                <a href="<?php the_field('social_media_2');  ?>" target="_blank">
                  <i class="<?php the_field('social_media_icon_2'); ?>"></i>
                </a>
                <a href="<?php the_field('social_media_3');  ?>" target="_blank">
                  <i class="<?php the_field('social_media_icon_3'); ?>"></i>
                </a>
                <a href="<?php the_field('social_media_4');  ?>" target="_blank">
                  <i class="<?php the_field('social_media_icon_4'); ?>"></i>
                </a>
              </div>
            <?php endwhile; wp_reset_postdata(); ?>
          </div>
          <!-- footer-1 -->

          <div class="footer-2">
            <p>Copyright &copy Mohammad Reza <?php the_date('Y'); ?></p>
          </div>
    </footer>
		<?php wp_footer(); ?>
  </body>
</html>
