<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Appscale</title>

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  
<header class="site-header">
  <div class="container">
    <div class="header-wrapper">
      <!-- logo -->
      <div class="logo">
				<a href="<?php echo esc_url(home_url('/')); ?>"> 
				<!-- Load custom logo -->
        <?php
          if (function_exists('the_custom_logo')) {
            the_custom_logo();
          }
        ?>
      </a>
    </div>
    <!-- logo -->

    <div class="main-menu">
			<!-- Print the menu -->
			<?php
				$args = array(
					'theme_location' => 'main-menu',      
					'container' => 'nav',
					'container_class' => 'site-nav'
				);
				wp_nav_menu($args);
			?>
    </div>
    <!-- main-menu -->

    <div class="hamburger-menu">
      <a href="#">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30" height="30" focusable="false">
          <title>Menu</title>
          <path stroke="#57b6f9" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" d="M4 7h22M4 15h22M4 23h22"></path>
        </svg>
      </a>
    </div>
    </div>
    <!-- Header wrapper -->
  </div>
  <!-- container -->
</header>